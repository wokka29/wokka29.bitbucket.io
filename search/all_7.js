var searchData=
[
  ['gain_0',['Gain',['../class_lab0x04__closed_loop_1_1_closed_loop.html#a7471c9a39d51c9bdecff557fc8de8f63',1,'Lab0x04_closedLoop::ClosedLoop']]],
  ['get_1',['get',['../classshares_1_1_queue.html#a45835daf8ee60391cca9667a942ade25',1,'shares.Queue.get(self)'],['../classshares_1_1_queue.html#a45835daf8ee60391cca9667a942ade25',1,'shares.Queue.get(self)']]],
  ['get_5fcalconstants_2',['get_calConstants',['../class_b_n_o055_1_1_b_n_o055.html#ac6bc0d6f23d4beb8de1eb7a7ce0abb07',1,'BNO055::BNO055']]],
  ['get_5fcalstatus_3',['get_calStatus',['../class_b_n_o055_1_1_b_n_o055.html#a981f1149265ae4332a26face112b2a10',1,'BNO055::BNO055']]],
  ['get_5fdata_4',['get_data',['../class_b_n_o055_1_1_b_n_o055.html#a7bdf291e305081bf5d2d31a5712436ac',1,'BNO055::BNO055']]],
  ['get_5fdelta_5',['get_delta',['../class_lab0x02__encoder_1_1_encoder.html#a5b7535e8d6aa89086bfced191be1e687',1,'Lab0x02_encoder.Encoder.get_delta()'],['../class_lab0x03__encoder_1_1_encoder.html#a009943a59452f4a7725d2643e335f428',1,'Lab0x03_encoder.Encoder.get_delta()'],['../classencoder_1_1_encoder.html#a2f451b6cb3e85e03d45e0ac097e29a29',1,'encoder.Encoder.get_delta()']]],
  ['get_5fposition_6',['get_position',['../class_lab0x02__encoder_1_1_encoder.html#ace76beddaf725521dabeeb5a70a34bdf',1,'Lab0x02_encoder.Encoder.get_position()'],['../class_lab0x03__encoder_1_1_encoder.html#a1781ca88041cdec5dce2770a62585e6d',1,'Lab0x03_encoder.Encoder.get_position()'],['../classencoder_1_1_encoder.html#abc44b0bb3d2ee93571f00d6bab5e7c53',1,'encoder.Encoder.get_position()']]],
  ['get_5fspeed_7',['get_speed',['../class_lab0x03__encoder_1_1_encoder.html#a95036f5fedd6841bc7735cd7eb2faf6a',1,'Lab0x03_encoder.Encoder.get_speed()'],['../classencoder_1_1_encoder.html#a479c2f80db370cb13855da30e54442b4',1,'encoder.Encoder.get_speed()']]],
  ['gyr_5fstat_8',['gyr_stat',['../class_b_n_o055_1_1_b_n_o055.html#a50fbe8800a99215a723e6fe114a3550c',1,'BNO055::BNO055']]]
];
