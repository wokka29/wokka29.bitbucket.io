var searchData=
[
  ['ball_20balancing_20platform_20_2d_20hand_20calculations_0',['Ball Balancing Platform - Hand Calculations',['../page4.html',1,'']]],
  ['ball_20balancing_20platform_20_2d_20simulation_1',['Ball Balancing Platform - Simulation',['../page5.html',1,'']]],
  ['bno055_2',['BNO055',['../class_b_n_o055_1_1_b_n_o055.html',1,'BNO055.BNO055'],['../class_lab0x05___b_n_o055_1_1_b_n_o055.html',1,'Lab0x05_BNO055.BNO055']]],
  ['brt_3',['brt',['../_m_e305___lab0x01_8py.html#a19eaabc4069130f034d6f80c7bc44b7e',1,'ME305_Lab0x01']]],
  ['buf_4',['buf',['../class_b_n_o055_1_1_b_n_o055.html#a4cdc5f6519326ca1411218bb2f4aed56',1,'BNO055::BNO055']]],
  ['bufcal_5',['bufcal',['../class_b_n_o055_1_1_b_n_o055.html#a8fa7cd3dca896367ce498d7ddbb4863f',1,'BNO055::BNO055']]],
  ['bus_5faddr_6',['bus_addr',['../class_lab0x05___b_n_o055_1_1_b_n_o055.html#a81d83795e88452f725ccaeb73c784b1e',1,'Lab0x05_BNO055.BNO055.bus_addr()'],['../class_b_n_o055_1_1_b_n_o055.html#a1a56ada940962c6f34fbc7c7f34ba2df',1,'BNO055.BNO055.bus_addr()']]],
  ['buttonint_7',['ButtonInt',['../_m_e305___lab0x01_8py.html#ac6845bb4804f3186d04fc4ba788d8df5',1,'ME305_Lab0x01']]],
  ['buttonpressed_8',['buttonPressed',['../_m_e305___lab0x01_8py.html#a0a066a89015f8df7854ae07d4d61ac9f',1,'ME305_Lab0x01']]]
];
