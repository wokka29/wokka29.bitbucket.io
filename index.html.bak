<!doctype html>
<html lang="en">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <meta name="description" content="">
        <meta name="author" content="TemplateMo">

        <title>Logan Williamson's Portfolio</title>

        <!-- CSS FILES -->
        <link rel="preconnect" href="https://fonts.googleapis.com">
        
        <link rel="preconnect" href="https://fonts.gstatic.com" crossorigin>
        
        <link href="https://fonts.googleapis.com/css2?family=DM+Sans:wght@400;500;700&display=swap" rel="stylesheet">

        <link href="css/bootstrap.min.css" rel="stylesheet">

        <link href="css/bootstrap-icons.css" rel="stylesheet">

        <link href="css/magnific-popup.css" rel="stylesheet">

        <link href="css/templatemo-first-portfolio-style.css" rel="stylesheet">
        
<!--

TemplateMo 578 First Portfolio

https://templatemo.com/tm-578-first-portfolio

-->
    </head>
    
    <body>

        <section class="preloader">
            <div class="spinner">
                <span class="spinner-rotate"></span>    
            </div>
        </section>

        <nav class="navbar navbar-expand-lg">
            <div class="container">

                <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
                    <span class="navbar-toggler-icon"></span>
                </button>

                <a href="index.html" class="navbar-brand mx-auto mx-lg-0">Logan Williamson</a>

                <div class="d-flex align-items-center d-lg-none">
                    <i class="navbar-icon bi-telephone-plus me-3"></i>
                    <a class="custom-btn btn" href="#section_4">
                        530-386-5364
                    </a>
                </div>

                <div class="collapse navbar-collapse" id="navbarNav">
                    <ul class="navbar-nav ms-lg-5">
                        <li class="nav-item">
                            <a class="nav-link click-scroll" href="#section_1">Home</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link click-scroll" href="#section_2">About</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link click-scroll" href="#section_3">Projects</a>
                        </li>

                        <li class="nav-item">
                            <a class="nav-link click-scroll" href="#section_4">Contact</a>
                        </li>
                    </ul>

                    <div class="d-lg-flex align-items-center d-none ms-auto">
                        <i class="navbar-icon bi-telephone-plus me-3"></i>
                        <a class="custom-btn btn" href="#section_4">
                            530-386-5364
                        </a>
                    </div>
                </div>
            </div>
        </nav>

        <main>

            <section class="hero d-flex justify-content-center align-items-center" id="section_1">
                <div class="container">
                    <div class="row">

                        <div class="col-lg-7 col-12">
                            <div class="hero-text">
                                <div class="hero-title-wrap d-flex align-items-center mb-4">
                                    <img src="SumoBot_Render.png" class="avatar-image avatar-image-large img-fluid" alt="">

                                    <h1 class="hero-title ms-3 mb-0">Mechatronics Portfolio</h1>
                                </div>

                                <h2 class="mb-4">A digital portfolio showcasing my mechatronics projects and relevant engineering skills</h2>
                            </div>
                        </div>
                    </div>
                </div>

                <svg xmlns="http://www.w3.org/2000/svg" viewBox="0 0 1440 320"><path fill="#535da1" fill:white fill-opacity="1" d="M0,160L24,160C48,160,96,160,144,138.7C192,117,240,75,288,64C336,53,384,75,432,106.7C480,139,528,181,576,208C624,235,672,245,720,240C768,235,816,213,864,186.7C912,160,960,128,1008,133.3C1056,139,1104,181,1152,202.7C1200,224,1248,224,1296,197.3C1344,171,1392,117,1416,90.7L1440,64L1440,0L1416,0C1392,0,1344,0,1296,0C1248,0,1200,0,1152,0C1104,0,1056,0,1008,0C960,0,912,0,864,0C816,0,768,0,720,0C672,0,624,0,576,0C528,0,480,0,432,0C384,0,336,0,288,0C240,0,192,0,144,0C96,0,48,0,24,0L0,0Z"></path></svg>
            </section>


            <section class="about section-padding" id="section_2">
                <div class="container">
                    <div class="row">

                        <div class="col-lg-6 col-12">
                            <img src="sequoia_fullshot.jpg" class="about-image img-fluid" alt="">
                        </div>

                        <div class="col-lg-6 col-12 mt-5 mt-lg-0">
                            <div class="about-thumb">

                                <div class="section-title-wrap d-flex justify-content-end align-items-center mb-4">
                                    <h2 class="text-white me-4 mb-0">My Background</h2>
                                </div>

                                <p>I am a mechanical engineering undergraduate senior concentrating in mechatronics at Cal Poly (expected graduation December 2022). My primary career interests are robotics and mechatronics. In particular, I am interested in medical and agricultural robotics used in precise positioning and delicate material handling applications.</br></br>
								My relevant skills include SolidWorks and Fusion 360 CAD and FEA, Fusion 360 (Eagle) PCB design, Python programming, C/C++ programming, MATLAB dynamic system modelling, failure modes and effects analysis, statics and strength of materials, and system dynamics. Additionally, I am a skilled technical writer and communicator; I diligently document my work and am adept at discussing technical aspects of a project within a design team.</br></br>
								Aside from my personal engineering projects, I spend my free time hiking, camping, rock climbing, mountain biking, and cooking. I particularly enjoy baking sourdough bread using my naturally cultivated sourdough starter. I am also casually learning Spanish to build upon the basics I learned in high school, with the goal of becoming conversationally proficient by 2024.
								</p>
                 
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="projects section-padding" id="section_3">
                <div class="container">
				
                    <div class="row">
                        <div class="col-lg-8 col-md-8 col-12 ms-auto">
                            <div class="section-title-wrap d-flex justify-content-center align-items-center mb-4">
                                <img src="Sumo_PCB.jpg" class="avatar-image img-fluid" alt="">

                                <h2 class="text-white ms-4 mb-0">Projects</h2>
                            </div>
                        </div>
					</div>
					
					<div class="row">
                        <div class="clearfix"></div>
						<div class="col-lg-12 col-md-12 col-12">
                            <div class="projects-thumb">
								
									<div class="projects-description">
										
										<h3 style="color:#5D3FD3" class="projects-title">Sumo Robot Control System Design Competition</h3>
											
											<p style="color:black">
												<ul>
													<li>Five teams of graduate students</li>
													<li>Eight week design cycle project</li>
													<li>Strictly rapid-prototyping manufacturing techniques</li>
													<li>Design, build, and program a prototype sumo robot</li>
													<li>Custom PCB and electronics design</li>
													<li>Compete for the title of best control system</li>													
												</ul>
							
											</p>
											
											<p>Results: </p>
										<p style="color:black">Successfully designed, built, programmed, tested, and documented a fully autonomous sumo robot. This prototype outperformed all four other robots entered in a tournament against ours.</p>
									</div>
								<div class="projects-placard">
										<a href="https://wokka29.bitbucket.io/SumoBot/sumobotProject.html">
											<img src="Sumo_Mech_Build.jpg" class="projects-image img-fluid" alt="">
										</a>
								</div>
                            </div>
                        </div>
					</div>
					
					<div class="row">
                        <div class="col-lg-6 col-md-6 col-12">
                            <div class="projects-thumb">
                                <div class="projects-info">
                                    <h3 style="color:#5D3FD3" class="projects-title">Ball Balancing Platform</h3>
									<h4 style="font-size:20px; color:#5D3FD3" class="projects-title">Intro to Mechatronics class project</h4>
									
									<p> Learning objectives include:
									<ul>
									<li>Understand high-level design of mechatronics systems</li>
									<li>Design software to meet real-time constraints</li>
									<li>Work effectively within a development team</li>
									<li>Manage team source code repository and project documentation website</li>
									</ul>
									</p>
									
									<p>Results: </p>
									<p style="font-size:13px; color:black">Successfully implemented two-layer cascading PD control algorithm and tuned the system to effectively balance a steel ball.</p>
									
									<a href="balancingPlatform.html">
										<img src="Ball_Balancing_Platform.png" class="projects-image img-fluid" alt="">
									</a>
                                </div>
                            </div>
                        </div>

                        <div class="col-lg-6 col-md-6 col-12">
                            <div class="projects-thumb">
                                <div class="projects-info">
                                    <h3 style="color:#5D3FD3" class="projects-title">Polar Plotting Machine</h3>
									<h4 style="font-size:20px; color:#5D3FD3" class="projects-title">Mechatronics class project</h4>
									
									<p> Learning objectives include:
										<ul>
											<li>Perform high-level design of a mechatronics system</li>
											<li>Design complex programs using an organized methodology</li>
											<li>Utilize hardware documentation to inform your design process</li>
											<li>Document hardware and software designs thoroughly</li>
										</ul>
									</p>
									
									<p>Results: </p>
									<p style="font-size:13px; color:black">Successfully designed, built, and programmed a robot capable of autonomously drawing images from Hewlett-Packard Graphics Language files.</p>
									
                                </div>

                                <a href="https://jarodlyles.github.io/">
                                    <img src="Polar_Plotter.png" class="projects-image img-fluid" alt="">
                                </a>
                            </div>
                        </div>
                    </div>
                </div>
            </section>

            <section class="contact section-padding" id="section_4">
                    <div class="container">
                        <div class="row">

                            <div class="col-lg-6 col-md-8 col-12">
                                <div class="section-title-wrap d-flex justify-content-center align-items-center mb-5">
                                    <img src="LinkedIn_Profile_Thumbnail.png" class="avatar-image img-fluid" alt="">

                                    <h2 class="text-white ms-4 mb-0">Contact Logan</h2>
                                </div>
                            </div>

                            <div class="clearfix"></div>

                            <div class="col-lg-3 col-md-6 col-12 pe-lg-0">
                                <div class="contact-info contact-info-border-start d-flex flex-column">
                                    <strong class="site-footer-title d-block mb-3">Relevant Skills</strong>

                                    <ul class="footer-menu">
                                        <li class="footer-menu-item"><a href="#" class="footer-menu-link">Mechatronics</a></li>

                                        <li class="footer-menu-item"><a href="#" class="footer-menu-link">SolidWorks</a></li>

										<li class="footer-menu-item"><a href="#" class="footer-menu-link">Fusion 360</a></li>

                                        <li class="footer-menu-item"><a href="#" class="footer-menu-link">C</a></li>
										
										<li class="footer-menu-item"><a href="#" class="footer-menu-link">C++</a></li>
										
                                        <li class="footer-menu-item"><a href="#" class="footer-menu-link">Python</a></li>

										<li class="footer-menu-item"><a href="#" class="footer-menu-link">MS Office</a></li>

                                    </ul>
                                </div>
                            </div>

                            <div class="col-lg-3 col-md-6 col-12 ps-lg-0">
                                <div class="contact-info d-flex flex-column">
                                    <strong class="site-footer-title d-block mb-3">About</strong>

                                    <p class="mb-2">
                                        I expect to graduate in December 2022 and am currently seeking employment. Please feel free to contact me.
                              </p>

                                    <strong class="site-footer-title d-block mt-4 mb-3">Email</strong>

                                    <p>
                                        <a href="mailto:logan.williamson29@gmail.com">
                                            logan.williamson29@gmail.com
                                        </a>
                                    </p>

                                    <strong class="site-footer-title d-block mt-4 mb-3">Call</strong>

                                    <p class="mb-0">
                                        <a href="tel: 530-386-5364">
                                            530-386-5364
                                        </a>
                                    </p>
                                </div>
                            </div>

                            <div class="col-lg-6 col-12 mt-5 mt-lg-0">
                                <form action="#" method="get" class="custom-form contact-form" role="form">
                                    <div class="row">
                                        <div class="col-lg-6 col-md-6 col-12">
                                            <div class="form-floating">
                                                <input type="text" name="name" id="name" class="form-control" placeholder="Name" required="">
                                                
                                                <label for="floatingInput">Name</label>
                                            </div>
                                        </div>

                                        <div class="col-lg-6 col-md-6 col-12"> 
                                            <div class="form-floating">
                                                <input type="email" name="email" id="email" pattern="[^ @]*@[^ @]*" class="form-control" placeholder="Email address" required="">
                                                
                                                <label for="floatingInput">Email address</label>
                                            </div>
                                        </div>

                                        <div class="col-lg-12 col-12">
                                            <div class="form-floating">
                                                <textarea class="form-control" id="message" name="message" placeholder="Tell me about the role"></textarea>
                                                
                                                <label for="floatingTextarea">Tell me about the role</label>
                                            </div>
                                        </div>

                                        <div class="col-lg-3 col-12 ms-auto">
                                            <button type="submit" class="form-control">Send</button>
                                        </div>

                                    </div>
                                </form>
                            </div>

                        </div>
                    </div>
            </section>

        </main>

        <footer class="site-footer">
            <div class="container">
                <div class="row">

                    <div class="col-lg-12 col-12">
                        <div class="copyright-text-wrap">
                            <p class="mb-0">
                                <span class="copyright-text">Copyright © 2036 <a href="#">First Portfolio</a> Company. All rights reserved.</span>
                                Design: 
                                <a rel="sponsored" href="https://templatemo.com" target="_blank">TemplateMo</a>
                            </p>
                        </div>
                    </div>

                </div>
            </div>
        </footer>

        <!-- JAVASCRIPT FILES -->
        <script src="js/jquery.min.js"></script>
        <script src="js/bootstrap.min.js"></script>
        <script src="js/jquery.sticky.js"></script>
        <script src="js/click-scroll.js"></script>
        <script src="js/jquery.magnific-popup.min.js"></script>
        <script src="js/magnific-popup-options.js"></script>
        <script src="js/custom.js"></script>

    </body>
</html>